

document.getElementById("1").addEventListener("click",function(){

    var hora = (new Date()).getHours();
    var minutos = (new Date()).getMinutes();
    var horax = hora * 3600
    var minutosx = minutos * 60
    var segundos = horax + minutosx
    alert('La hora actual es:' +hora+':'+minutos+'\nLa hora actual en segundos es:'+segundos);
    
})


document.getElementById("2").addEventListener("click",function(){

    var base = parseFloat(prompt('Ingrese el valor de la base:'))
    var altura = parseFloat(prompt('Ingrese el valor de la altura:'))
    var area = (base * altura) /2;
    alert('El área del triángulo es:'+ area);
})

document.getElementById("3").addEventListener("click",function(){

    var numero = parseFloat(prompt('Ingrese el valor a calcular'))
    if(numero % 2 != 0) {
        resultado = Math.sqrt(numero).toFixed(2);
        return alert('La raíz cuadrada es:' + resultado);
      }
      else {
        return alert('Es un número par')
      }
    }
)

document.getElementById("4").addEventListener("click",function(){

    var texto = prompt('Ingrese un texto:')
    alert('Texto:'+ texto +'\nEl largo del texto es:'+texto.length +' '+ 'caracteres')
})


document.getElementById("5").addEventListener("click",function(){

    var array1 = new Array('Lunes','Martes','Miércoles','Jueves','Viernes')
    var array2 = new Array('Sabado','Domingo')
    var array3 = array1.concat(array2);

    alert('Dias hábiles:'+ array1 + '\nFines de semana:'+array2 + '\nLos dias de la semana:'+ array3)
})

document.getElementById("6").addEventListener("click",function(){

    alert('La versión del Navegador es:'+ navigator.appVersion)
})

document.getElementById("7").addEventListener("click",function(){

    alert('El ancho de la pantalla es:'+screen.width +'px'+ '\nLa altura de la pantalla es:'+screen.height+'px')
})

document.getElementById("8").addEventListener("click",function(){

    window.print();
})