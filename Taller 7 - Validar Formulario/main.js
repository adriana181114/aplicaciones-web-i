
function Validar(){
    valor = document.getElementById("cedula").value;
    valor1 = document.getElementById("apellido").value;
    valor2 = document.getElementById("nombre").value;
    valor3 = document.getElementById("estado").selectedIndex;
    valor4 = document.getElementById("sexo").selectedIndex;
    valor5 = document.getElementById("ciudad").value;
    valor6 = document.getElementById("direc").value;
    valor7 = document.getElementById("telefono").value;
    valor8 = document.getElementById("correo").value;

    if( valor == null || valor.length == 0){
        alert("Este campo Nº Cédula es obligatorio");
        return false;
    }
    else if (isNaN (valor)){
        alert("El campo Nº Cédula tiene que ser numérico");
        return false;
    }
    else if( valor1 == null || valor1.length == 0){
        alert("Este campo Apellido es obligatorio");
        return false;
    }
    else if( valor2 == null || valor2.length == 0){
        alert("Este campo Nombre es obligatorio");
        return false;
    }
    else if( valor3 == null || valor3 == 0){
        alert("Este campo  Estado Civil es obligatorio");
        return false;
    }
    else if( valor4 == null || valor4 == 0){
        alert("Este campo Sexo es obligatorio");
        return false;
    }
    else if( valor5 == null || valor5.length == 0){
        alert("Este campo Ciudad es obligatorio");
        return false;
    }
    else if( valor6 == null || valor6.length == 0){
        alert("Este campo Dirección es obligatorio");
        return false;
    }
    else if( valor7 == null || valor7.length == 0){
        alert("Este campo Teléfono es obligatorio");
        return false;
    }
    else if(isNaN (valor7)){
        alert("El campo Teléfono tiene que ser numérico");
        return false;
    }
    else if( valor8 == null || valor8.length == 0){
        alert("Este campo  Correo electrónico es obligatorio");
        return false;
    }
    else if(!(/^[^@]+@[^@]+.[a-zA-Z]{2,7}$/.test(valor8))){
    alert ("Correo electrónico incorrecto")
    return false;
    }
    setCookie('CI', valor, 1);
    setCookie('lastname', valor1, 1);
    setCookie('name', valor2, 1);
    setCookie('status', valor3, 1);
    setCookie('sex', valor4, 1);
    setCookie('city', valor5, 1);
    setCookie('address', valor6, 1);
    setCookie('phone', valor7, 1);
    setCookie('mail', valor8, 1);
    alert('El formulario se envió con éxito.');
    return true;
}
function setCookie(nombre, valores, fecha){
    var valorFecha = new Date();
    valorFecha.setTime(valorFecha.getTime()+fecha*24*60*60*1000);
    var fecha = 'expires='+valorFecha.toUTCString();
    document.cookie = nombre+'='+valores+';'+fecha+';path=/';
}  
function getCokkie(mostrarNombre){
    var valorCookie = mostrarNombre+'=';
    var array = document.cookie.split(';');
    for(let i=0; i<array.length; i++){
       var c = array[i];
       while (c.charAt(0)==' '){
         c = c.substring(1);
       }
       if(c.indexOf(mostrarNombre)==0){
         return c.substring(valorCookie.length, c.length);
       }
    }
    return " ";
}
document.getElementById('cookiesx').addEventListener('click',MostrarCookie);
function MostrarCookie(){
    if(document.cookie==""){
        alert('No existen cookies en el navegador.');
    }
    else{
        var cedula = getCokkie('CI');
        var apellido = getCokkie('lastname');
        var nombre = getCokkie('name');
        var estadocv = getCokkie('status');
        var sexo = getCokkie('sex');
        var ciudad = getCokkie('city');
        var direccion = getCokkie('address');
        var telefono = getCokkie('phone');
        var correo = getCokkie('mail');
        alert("Cedula: " + cedula + "\nApellido:" + apellido +
        "\nNombre:" + nombre + "\nEstado Civil:" + estadocv +
        "\nSexo:" + sexo + "\nCiudad:" + ciudad + "\nDirección:" +
         direccion + "\nTelefono:" + telefono + "\nCorreo:" + 
         correo );
    }
}