document.getElementById("1").addEventListener("click", ()=>{
    var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
    "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

    for(var i=0; i<meses.length; i++){
        document.getElementById('resultado1').insertAdjacentHTML('beforebegin', `<p class="mes">${meses[i]}</p>`);
    } 
    document.getElementById("caja1").classList.add("cajax");
    document.getElementById("opt").classList.add("cajay");
});

document.getElementById("2").addEventListener("click", ()=>{
    function imprimeDatos(){
        document.getElementById('resultado2').innerHTML +=('Codigo:' + this.codigo + '<br>' +
        'Nombre:'+this.nombre+ '<br>' + 'Precio:'+ this.precio + '<br>' +'--------'+'<br>');
    }
    function Producto_alimenticio (codigo,nombre,precio){
        this.codigo = codigo;
        this.nombre = nombre;
        this.precio = precio;
        this.imprimeDatos = imprimeDatos;
    }
    var productos = new Array(3);
    productos[0] = new Producto_alimenticio('0001','Banana','0.25');
    productos[1] = new Producto_alimenticio('0002','Arroz','1.10');
    productos[2] = new Producto_alimenticio('0003','Papa','0.50');

    for(var i=0; i<productos.length; i++){
        var p = document.createElement("p");
        p.innerHTML = productos[i].imprimeDatos();
    }
    document.getElementById("caja2").classList.add("caja2x");
})